package com.example.kadijak;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Jeu extends AppCompatActivity {

    private List<Joueur> joueurs; //Liste des joueurs
    private int joueurCourant; // Index du joueur actuel
    private ProgressBar mProgressBar; // Bar de progression
    private TextView time; // Texte du timer
    private CountDownTimer timer; // Timer
    private TextView nomJoueur; // Nom du joueur
    private BddHandler db; // Lien avec la bdd

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing_defi);

        //Initialisation des variables
        this.joueurs = new ArrayList<Joueur>();
        this.db = new BddHandler(this);

        //récupération des joueurs
        recupererJoueurs(this.getIntent());

        //On vérifie que la bdd est pleine
        if(db.nbTotalDefi() == 0)
            db.remplirDb();

        //On récupère un défi aléatoire
        Defi d = this.getRandomDefi(this.db);

        //On gère l'affichage
        this.time = (TextView)this.findViewById(R.id.timer);
        this.gestionProgressBar();
        this.gestionTimer(d.getTempsdevalidation());
        this.nomJoueur = (TextView)this.findViewById(R.id.playerName);
        String s = joueurs.get(this.joueurCourant).getNomJoueur();
        this.nomJoueur.setText(s);
        TextView contenu = (TextView)this.findViewById(R.id.contenuDefi);
        contenu.setText(d.getContenu());
    }

    //réinitialise l'affichage
    public void update() {
        // Condition de victoire
        if(this.joueurs.size() > 1) {

            Defi d = this.getRandomDefi(this.db);
            this.gestionTimer(d.getTempsdevalidation());
            this.nomJoueur = (TextView) this.findViewById(R.id.playerName);
            String s;

            //On adapte l'affichage en fonction de l'etat du joueur
            if(joueurs.get(this.joueurCourant).aJoker()){
                s = joueurs.get(this.joueurCourant).getNomJoueur();
            }else{
                s = joueurs.get(this.joueurCourant).getNomJoueur() + " " +this.getResources().getString(R.string.derniere);
            }
            this.nomJoueur.setText(s);
            TextView contenu = (TextView) this.findViewById(R.id.contenuDefi);
            contenu.setText(d.getContenu());
        }else {
            //La partie est terminée
            Intent i = new Intent(this, Victoire.class);
            i.putExtra("gagnant", this.joueurs.get(0).getNomJoueur());
            startActivity(i);
        }
    }

    //Récupère un défi aléatoire dans la bdd
    public Defi getRandomDefi(BddHandler db) {
        Random r = new Random();
        return db.getDefi(r.nextInt(db.nbTotalDefi() + 1));
    }

    //Va à l'activité Reussite avec un retour attendu
    public void reussite(View v) {
        Intent i = new Intent(this, Reussite.class);
        i.putExtra("NomJoueur", this.joueurs.get(this.joueurCourant).getNomJoueur());
        this.startActivityForResult(i, 1);
    }

    //Va à l'activité Echec avec un retour attendu
    public void echec(View v) {
        Intent i = new Intent(this, Echec.class);
        i.putExtra("NomJoueur", this.joueurs.get(this.joueurCourant).getNomJoueur());
        this.startActivityForResult(i, 1);
    }

    // Passe au joueur suivant dans la liste
    public void joueurSuivant() {
        if(this.joueurCourant == this.joueurs.size() - 1) {
            this.joueurCourant = 0;
        }else {
            this.joueurCourant += 1;
        }
    }

    //Desactivation du bouton retour
    @Override
    public void onBackPressed() { }

    //Traitement du retour d'activité
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                switch (data.getIntExtra("valeur", -1)) {
                    // Le joueur n'a pas complété le défi
                    case 0:
                        if (this.joueurs.get(this.joueurCourant).aJoker()) {
                            this.joueurs.get(this.joueurCourant).setJoker(false);
                        } else {
                            this.joueurs.remove(this.joueurCourant);
                            this.joueurCourant -= 1;
                        }
                        this.joueurSuivant();
                        this.timer.cancel();
                        this.update();
                        break;
                        //Le joueur a complété le défi
                    case 1:
                        this.joueurSuivant();
                        this.timer.cancel();
                        this.update();
                        break;
                }
            }
        }
    }

    //Recupère les valeurs de l'activité précédente
    public void recupererJoueurs(Intent i) {
        for (int j = 0; j < i.getIntExtra("size",0); j++) {
            this.joueurs.add(new Joueur(i.getStringExtra("nom"+j), i.getBooleanExtra("joker"+j,false)));
        }
        this.joueurCourant = i.getIntExtra("joueurCourant",0);
    }

    //Gère le texte du timer
    public void gestionTimer(int tempsdevalidation) {
        timer = new CountDownTimer(tempsdevalidation*1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String s = new Long(millisUntilFinished/1000).toString();
                Jeu.this.time.setText(s);
            }
            public void onFinish() {
                this.cancel();
                Jeu.this.echec(new View(Jeu.this.getApplicationContext()));
            }
        }.start();
    }

    //Gère la barre de progression autour du timer
    public void gestionProgressBar() {
        CountDownTimer mCountDownTimer;
        int i=0;
        this.mProgressBar=(ProgressBar)findViewById(R.id.progressBar);
        this.mProgressBar.setProgress(i);
        mCountDownTimer=new CountDownTimer(5000,1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                int i = Jeu.this.mProgressBar.getProgress();
                Log.v("Log_tag", "Tick of Progress"+ i+ millisUntilFinished);
                i++;
                Jeu.this.mProgressBar.setProgress((int)i*100/(5000/1000));

            }

            @Override
            public void onFinish() {

            }
        };
        mCountDownTimer.start();
    }
}
