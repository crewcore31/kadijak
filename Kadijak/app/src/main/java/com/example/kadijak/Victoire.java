package com.example.kadijak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Victoire extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_victory);
        TextView t = (TextView)this.findViewById(R.id.forniteEpicWin);
        t.setText(this.getIntent().getStringExtra("gagnant"));
    }

    public void retourMenu(View v) {
        Intent i = new Intent(this, Menu.class);
        this.startActivity(i);
    }
    public void rejouer(View v) {
        Intent i = new Intent(this, SaisieJoueurs.class);
        this.startActivity(i);
    }
    // Desactivation du bouton retour
    @Override
    public void onBackPressed() { }

}
