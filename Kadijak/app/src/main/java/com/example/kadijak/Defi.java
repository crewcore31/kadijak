package com.example.kadijak;

public class Defi {

    private String contenu; // Le texte du défi
    private int tempsdevalidation; // Temps requis pour réaliser le défi
    private int ID; // Identifiant du défi

    public Defi(int ID, String contenu, int tempsdevalidation) {
        this.ID = ID;
        this.contenu = contenu;
        this.tempsdevalidation = tempsdevalidation;
    }

    // Retourne le contenu du défi
    public String getContenu() {
        return this.contenu;
    }

    // Retourne le temps de validation d'un défi
    public int getTempsdevalidation() {
        return this.tempsdevalidation;
    }

    // Retourne l'identifiant d'un défi
    public int getID() {
        return this.ID;
    }
}
