package com.example.kadijak;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Locale;

public class Settings extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private BddHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_settings);
        radioGroup = findViewById(R.id.langSelect);
        this.db = new BddHandler(this);
    }

    public void gotoMenu() {
        Intent i = new Intent(this, Menu.class);
        this.startActivity(i);
    }

    public void checkButton(View v){
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);

        switch (radioButton.getId()){
            case R.id.radioFR:
                //lang fr
                Toast.makeText(this, "Français", Toast.LENGTH_SHORT).show();
                Locale myLocale = new Locale("fr");
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);
                this.db.updateDb();
                break;
            case R.id.radioEN:
                //lang en
                Toast.makeText(this, "English", Toast.LENGTH_SHORT).show();
                Locale myLocale1 = new Locale("en");
                Resources res1 = getResources();
                DisplayMetrics dm1 = res1.getDisplayMetrics();
                Configuration conf1 = res1.getConfiguration();
                conf1.locale = myLocale1;
                res1.updateConfiguration(conf1, dm1);
                this.db.updateDb();
                break;
        }
    }

    public void Validate(View v){
        this.gotoMenu();
    }

}
