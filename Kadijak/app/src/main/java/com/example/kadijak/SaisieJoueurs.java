package com.example.kadijak;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SaisieJoueurs extends AppCompatActivity {

    private List<Joueur> joueurs; // Liste des joueurs
    private EditText joueurSaisie; // Nom du joueur que l'utilisateur entre
    private ListView affichageJoueurs; // Vue de la liste des joueurs
    private int lastItemSelected; //Dernier item de la liste de joueurs cliqué
    private static final int JOUEURSMAX = 8; // Nombres de joueurs max

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saisie_joueurs);

        this.lastItemSelected = -1;
        this.joueurs = new ArrayList<>();
        this.joueurSaisie = (EditText)this.findViewById(R.id.saisieJoueur);
        this.affichageJoueurs = (ListView)this.findViewById(R.id.Listview);

        // Gestion des cliques sur les items de la liste
        this.affichageJoueurs.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {

                SaisieJoueurs.this.setLastItemSelected(position);
                    for (int j = 0; j < SaisieJoueurs.this.joueurs.size(); j++) {
                        SaisieJoueurs.this.affichageJoueurs.getChildAt(j).setBackgroundColor(Color.WHITE);
                    }
                    SaisieJoueurs.this.affichageJoueurs.getChildAt(position).setBackgroundColor(Color.DKGRAY);
                }

        });
    }

    // Affichage des joueurs
    public void afficherJoueurs() {
        String[] tmp = new String[this.joueurs.size()];
        for (int i = 0; i < this.joueurs.size(); i++) {
            tmp[i] = this.joueurs.get(i).getNomJoueur();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SaisieJoueurs.this,
                android.R.layout.simple_list_item_1, tmp);
        this.affichageJoueurs.setAdapter(adapter);
    }

    // Ajoute le joueur écrit quand l'utilisateur appuie sur le bouton "créer"
    public void ecrireJoueur(View v) {
        if(!(this.joueurSaisie.getText().toString().trim().equals(""))) {
        if(this.joueurs.size() < SaisieJoueurs.JOUEURSMAX) {
            this.joueurs.add(new Joueur(this.joueurSaisie.getText().toString().trim(), true));
            this.afficherJoueurs();
        }else {
            Toast.makeText(this, "Le nombre maximal de joueur a été atteint", Toast.LENGTH_LONG).show();
        }

        this.joueurSaisie.setText("");
    }
    }
    public void setLastItemSelected(int pos) {
        this.lastItemSelected = pos;
    }

    // Permet de déterminer qui commence la partie
    public int determinerPremier() {
        Random r = new Random();
        return r.nextInt(this.joueurs.size());
    }

    // Supprime le dernier joueur sélectionné dans la liste (Bouton "supprimer")
    public void supprimerJoueur(View v) {
        if(this.joueurs.size() != 0) {
            this.joueurs.remove(this.lastItemSelected);
        }
        afficherJoueurs();
    }

    // Envoie tous les données a l'activité suivante
    public void envoyerJoueurs(Intent i) {
        for (int j = 0; j < this.joueurs.size(); j++) {
            i.putExtra("nom"+j, this.joueurs.get(j).getNomJoueur());
            i.putExtra("joker"+j, this.joueurs.get(j).aJoker());
        }
        i.putExtra("size", this.joueurs.size());
        i.putExtra("joueurCourant", this.determinerPremier());
    }

    // Passe a l'activité suivante
    public void gotoJeu(View v) {
        if(this.joueurs.size() > 1) {
            Intent i = new Intent(this, PreparePlayerActivity.class);
            this.envoyerJoueurs(i);
            this.startActivity(i);
        }else {
            Toast.makeText(this, "Nombre de joueurs insuffisant", Toast.LENGTH_LONG).show();
        }
    }

}
