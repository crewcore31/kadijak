package com.example.kadijak;

import android.os.Parcel;
import android.os.Parcelable;

public class Joueur {

    private String nomJoueur; //Nom du joueur
    private boolean joker; // Joker du joueur

    public Joueur(String nomJoueur, boolean joker) {
        this.nomJoueur = nomJoueur;
        this.joker = joker;
    }

    // Renvoie le joker du joueur
    public boolean aJoker() {
        return this.joker;
    }

    //Permet de modifier le joker du joueur
    public void setJoker(boolean b) {
        this.joker = b;
    }

    // Renvoie le nom du joueur
    public String getNomJoueur() {
        return this.nomJoueur;
    }

    // Permet de modifier le nom du joueur
    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

}
