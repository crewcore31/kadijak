package com.example.kadijak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;

public class Menu extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    // Mise en place du menu deroulant de l'app bar
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.rules:
                gotoRules();
                break;
            case R.id.settings:
                gotoSettings();
                break;
            case R.id.about:
                gotoAbout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    // Permet de quitter l'application
    public void leaveApplication(View v) {
        this.finish();
        System.exit(0);
    }
    public void gotoSaisieJoueurs(View v) {
        Intent i = new Intent(this, SaisieJoueurs.class);
        this.startActivity(i);
    }
    public void gotoSettings(){
        Intent i = new Intent(this, Settings.class);
        startActivity(i);
    }
    public void gotoRules(){
        Intent i = new Intent(this, Regles.class);
        startActivity(i);
    }
    public void gotoAbout(){
        Intent i = new Intent(this, About.class);
        startActivity(i);
    }
}
