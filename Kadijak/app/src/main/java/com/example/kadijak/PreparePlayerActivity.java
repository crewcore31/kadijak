package com.example.kadijak;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class PreparePlayerActivity extends AppCompatActivity {

    private List<Joueur> joueurs; // Liste des joueurs
    private int joueurCourant; // Index du joueur actuel
    private TextView nomJoueur; // Nom du joueur

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_prepare_player);
        this.joueurs = new ArrayList<>();
        this.recupererJoueurs(this.getIntent());
        this.nomJoueur = (TextView)this.findViewById(R.id.nomJoueur);
        String s = this.joueurs.get(this.joueurCourant).getNomJoueur();
        this.nomJoueur.setText(s);
    }

    // Lance le jeu
    public void gotoJeu(View v) {
        Intent i = new Intent(this, Jeu.class);
        this.envoyerJoueurs(i);
        this.startActivity(i);
    }

    // Envoie les données à l'activité suivante
    public void envoyerJoueurs(Intent i) {
        for (int j = 0; j < this.joueurs.size(); j++) {
            i.putExtra("nom" + j, this.joueurs.get(j).getNomJoueur());
            i.putExtra("joker"+j, this.joueurs.get(j).aJoker());
        }
        i.putExtra("size", this.joueurs.size());
        i.putExtra("joueurCourant", this.joueurCourant);
    }

    // Recupère les données de l'activité précedente
    public void recupererJoueurs(Intent i) {
        for (int j = 0; j < i.getIntExtra("size",0); j++) {
            this.joueurs.add(new Joueur(i.getStringExtra("nom"+j), i.getBooleanExtra("joker"+j,false)));
        }
        this.joueurCourant = i.getIntExtra("joueurCourant",0);
    }

}
