package com.example.kadijak;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Reussite extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_succes);
        TextView t = (TextView)this.findViewById(R.id.echec);
        t.setText(this.getResources().getString(R.string.bravo)+" " + this.getIntent().getStringExtra("NomJoueur")+" !");
    }

    //Retour de la valeur
    public void continuer(View v) {
        Intent i = new Intent();
        i.putExtra("valeur", 1);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

    //Desactivation du bouton retour
    @Override
    public void onBackPressed() { }
}
