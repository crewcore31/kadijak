package com.example.kadijak;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

public class BddHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "Defis.db";


    private Context context;

    public BddHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    //Initialise la bdd
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(this.context.getResources().getString(R.string.SQLCREATE));

    }

    // Vide le cache au besoin
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(this.context.getResources().getString(R.string.SQLDELETE));
        this.onCreate(sqLiteDatabase);
    }


    // Renvoie le nombre total de défi dans la bdd
    public int nbTotalDefi() {
        SQLiteDatabase db = this.getReadableDatabase();
        long result = DatabaseUtils.queryNumEntries(db,this.context.getResources().getString(R.string.TABLE_NAME));
        db.close();
        return (int)result;
    }


    // Permet de récupérer le Défi correspondant à l'ID passé en parametre dans la bdd
    public Defi getDefi(Integer id) {
        Defi d = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + this.context.getResources().getString(R.string.TABLE_NAME) +" WHERE ID = " + id + ";", null);
        if (cursor.moveToFirst()) {
            do {
                d = new Defi(cursor.getInt(cursor.getColumnIndexOrThrow(this.context.getResources().getString(R.string.COLUMN_ID))), cursor.getString(cursor.getColumnIndexOrThrow(this.context.getResources().getString(R.string.COLUMN_CONTENU))), cursor.getInt(cursor.getColumnIndexOrThrow(this.context.getResources().getString(R.string.COLUMN_TDV))));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return d;
    }

    // Remplit la bdd à partir du XML
    public void remplirDb() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] id = this.context.getResources().getStringArray(R.array.idDefi);
        String[] contenu = this.context.getResources().getStringArray(R.array.contenuDefi);
        String[] temps = this.context.getResources().getStringArray(R.array.tempsDefi);
        for (int i = 0; i < id.length; i++) {
            values.put(this.context.getResources().getString(R.string.COLUMN_ID), Integer.parseInt(id[i]));
            values.put(this.context.getResources().getString(R.string.COLUMN_CONTENU), contenu[i]);
            values.put(this.context.getResources().getString(R.string.COLUMN_TDV), Integer.parseInt(temps[i]));
            db.insert(this.context.getResources().getString(R.string.TABLE_NAME), null, values);
        }
    }


    public void updateDb() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String[] id = this.context.getResources().getStringArray(R.array.idDefi);
        String[] contenu = this.context.getResources().getStringArray(R.array.contenuDefi);
        String[] temps = this.context.getResources().getStringArray(R.array.tempsDefi);
        String where = this.context.getResources().getString(R.string.COLUMN_ID) + " = ";
        for (int i = 0; i < id.length; i++) {
            values.put(this.context.getResources().getString(R.string.COLUMN_ID), Integer.parseInt(id[i]));
            values.put(this.context.getResources().getString(R.string.COLUMN_CONTENU), contenu[i]);
            values.put(this.context.getResources().getString(R.string.COLUMN_TDV), Integer.parseInt(temps[i]));
            db.update(this.context.getResources().getString(R.string.TABLE_NAME), values, where + id[i], null);
        }
    }


}
